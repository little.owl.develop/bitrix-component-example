<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * Keys for use:
 *
 * ROOT_NAMESPACE
 * COMPONENT_NAME
 * COMPONENT_DESCRIPTION
 * PATH
 * LOC_PARAM_GROUPS
 * LOC_PARAMS
 * LOC_CLASS
 */

$componentVars = [];

{
    // SET YOUR VALUES HERE
    $rootNamespace = 'RPN';
    $componentName = 'FKKO_LIST';
    $childPath = [
        'fkko',
    ];
    // --------------------
    
    // Do not change that
    $componentVars['ROOT_NAMESPACE'] = $rootNamespace;
    
    $componentVars['COMPONENT_NAME'] = $rootNamespace
        . '_' . 'COMPONENT_NAME' . '_' .$componentName;
    
    $componentVars['COMPONENT_DESCRIPTION'] = $rootNamespace
        . '_' . 'COMPONENT_NAME' . '_' .$componentName . '_' . 'DESCRIPTION';
    
    $componentVars['LOC_PARAM_GROUPS'] = $componentVars['COMPONENT_NAME'] . '_' . 'PARAM_GROUPS' . '_';
    $componentVars['LOC_PARAMS'] = $componentVars['COMPONENT_NAME'] . '_' . 'PARAMS' . '_';
    
    $componentVars['LOC_CLASS'] = $componentVars['COMPONENT_NAME'] . '_' . 'CLASS' . '_';
    
    $componentVars['PATH'] = [
        [
            'ID' => $rootNamespace,
            'NAME' => 'PATH' . '_' . $rootNamespace . '_' . 'NAME',
        ],
    ];
    
    foreach ($childPath as $i => $id) {
        $componentVars['PATH'][$i + 1] = [
            'ID' => $id,
            'NAME' => 'PATH' . '_' . $id . '_' . 'NAME',
        ];
    }
}