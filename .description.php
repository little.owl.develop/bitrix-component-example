<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

include __DIR__ . DIRECTORY_SEPARATOR . '.namespace.php';

/** @var array $componentVars */

// Set component path
$pathCounter = count($componentVars['PATH']);
$path = [];
if ($pathCounter > 1) {
    $reversePath = array_reverse($componentVars['PATH']);
    $tmp = [];
    foreach ($reversePath as $i => $val) {
        $val = [
            'ID' => $val['ID'],
            'NAME' => Loc::getMessage($val['NAME']),
        ];
        
        if ($i) {
            $tmp = array_merge($val, ['CHILD' => $tmp]);
        } else {
            $tmp = $val;
        }
    }
    $path = $tmp;
} else {
    $path = [
        'ID' => $componentVars['PATH'][0]['ID'],
        'NAME' => Loc::getMessage($componentVars['PATH'][0]['NAME']),
    ];
}

$arComponentDescription = [
    'NAME' => Loc::getMessage($componentVars['COMPONENT_NAME']),
    'DESCRIPTION' => Loc::getMessage($componentVars['COMPONENT_DESCRIPTION']),
    // 'ICON' => '/images/sale_order_full.gif',
    'PATH' => $path,
];
