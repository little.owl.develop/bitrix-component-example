<?php
include __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR
    . '..' . DIRECTORY_SEPARATOR . '.namespace.php';

/** @var array $componentVars */

// ERRORS
$MESS[$componentVars['LOC_CLASS'] . 'ERROR' . '_' . 'UNKNOWN'] = 'Неизвестная ошибка';