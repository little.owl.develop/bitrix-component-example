<?php
include __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR
    . '..' . DIRECTORY_SEPARATOR . '.namespace.php';

/** @var array $componentVars */

// GROUPS
$MESS[$componentVars['LOC_PARAM_GROUPS'] . 'BASE'] = 'Основное';
$MESS[$componentVars['LOC_PARAM_GROUPS'] . 'PAGINATION'] = 'Пагинация';
$MESS[$componentVars['LOC_PARAM_GROUPS'] . 'ADDITIONAL'] = 'Дополнительное';

// PARAMS
$MESS[$componentVars['LOC_PARAMS'] . 'INCLUDE_SEARCH'] = 'Включить поиск';
$MESS[$componentVars['LOC_PARAMS'] . 'PER_PAGE'] = 'Кол-во элементов на странице';
$MESS[$componentVars['LOC_PARAMS'] . 'FILTER_CODES'] = 'Фильтр по кодам';