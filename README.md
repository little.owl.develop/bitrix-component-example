## Простой компонент bitrix
Пример простого компонента на битрикс, который получает список элементов из некой ORM со строкой поиска.

Файл **.namespace.php** создан для упрощения создания компонентов. В его начале вводятся название компонета и его путь, 
в соответствующих языковых файлах пишутся переводы, после чего внутри компонента можно подключить этот файл и 
использовать переменную **$componentVars**, например, для уникальных префиксов в языковых файлах.

JS в компоненте требует доработки для соответствия современным стандартам, но он здесь приведён только в 
ознакомительных целях, чтобы компонент работал так, как задумано.