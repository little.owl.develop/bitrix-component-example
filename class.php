<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use RPN\Fkko\Entity\ListTable;

Loc::loadMessages(__FILE__);

class FkkoListComponent extends CBitrixComponent
{
    /**
     * @param string $errorText
     * @return void
     */
    protected function showError($errorText)
    {
        echo '<div style="color: red;">' . $errorText . '</div>';
    }
    
    /**
     * @return mixed|void
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     */
    public function executeComponent()
    {
        include __DIR__ . DIRECTORY_SEPARATOR . '.namespace.php';
        /** @var array $componentVars */
        
        \Bitrix\Main\Loader::includeModule('rpn.fkko');
        
        // Получаем запрос и определяем, ajax или нет
        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        $this->arResult['AJAX'] = $request->get('AJAX') == 'Y' ? 'Y' : 'N';
        
        $this->arResult['INCLUDE_SEARCH'] = $this->arParams['INCLUDE_SEARCH'] == 'Y' ? 'Y' : 'N';
        
        $filter = [];
        
        if (!empty($this->arParams['FILTER_CODES'])) {
            $filter[] = [
                '=CODE' => $this->arParams['FILTER_CODES'],
            ];
        }
        
        // Фильтруем части кода (названия) элемента по поиску, если нужно
        $search = $request->get('search');
        if ($search) {
            $filter[] = [
                'LOGIC' => 'OR',
                'CODE' => '%' . $search . '%',
                'NAME' => '%' . $search . '%',
            ];
        }
        
        global $APPLICATION;
        /** @global CMain $APPLICATION */
        
        // Используется кеш по заданному фильтру
        if ($this->StartResultCache(false, $filter)) {
            // Объект навигации
            $navId = 'nav-more-fkko';
            $nav = new \Bitrix\Main\UI\PageNavigation($navId);
            $nav->allowAllRecords(false)
                ->setPageSize(intval($this->arParams['PER_PAGE']))
                ->initFromUri();
    
            $this->arResult['BASE_DIR'] = explode($navId, $APPLICATION->GetCurDir())[0];
            
            // Список элементов из ORM
            $elementsList = ListTable::getList(
                [
                    'filter' => $filter,
                    'count_total' => true,
                    'offset' => $nav->getOffset(),
                    'limit' => $nav->getLimit(),
                    'cache' => [
                        'ttl' => 2592000, // ~ 1 month
                    ],
                ]
            );
            
            $nav->setRecordCount($elementsList->getCount());
    
            // Навигация
            ob_start();
            $APPLICATION->IncludeComponent(
                'bitrix:main.pagenavigation',
                '',
                [
                    'NAV_OBJECT' => $nav,
                    'SEF_MODE' => 'Y',
                ],
                $this
            );
            $this->arResult['NAV_STRING'] = ob_get_contents();
            ob_end_clean();
    
            $this->arResult['ITEMS'] = $elementsList->fetchAll();
    
            $this->includeComponentTemplate();
        }
    }
}