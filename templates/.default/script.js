fkkoList = function (data) {
    this.BASE_DIR = data.BASE_DIR;
    this.INCLUDE_SEARCH = data.INCLUDE_SEARCH;
    this.$fkko = $('[data-js-id="fkko-component"]');
    this.ajaxReq = 'ToCancelPrevReq';
    
    this.init();
};

fkkoList.prototype.init = function () {
    let thisComponentObject = this;
    
    let $fkkoPagination = thisComponentObject.$fkko.find('[data-js-id="fkko-component-pagination"]');
    
    if ($fkkoPagination.length) {
        $fkkoPagination.find('a').bind('click', function (e) {
            e.preventDefault();
        
            let link = $(this).attr('href');
        
            $.ajax({
                url: link,
                method: 'POST',
                data: {
                    'AJAX': 'Y',
                },
                success: (result) => {
                    history.pushState({}, document.title, link);
                    thisComponentObject.$fkko.html(result);
                    thisComponentObject.init();
                },
            });
        });
    }
    
    if (thisComponentObject.INCLUDE_SEARCH == 'Y') {
        let $fkkoSearchInput = $('input[name="fkko-search"]');
        if ($fkkoSearchInput.length) {
            $fkkoSearchInput.bind('keyup change', function (e) {
                let val = $fkkoSearchInput.val();
                
                let link = thisComponentObject.BASE_DIR;
                if (val.length) {
                    link += '?search=' + $fkkoSearchInput.val();
                }
    
                thisComponentObject.ajaxReq = $.ajax({
                    url: link,
                    method: 'POST',
                    data: {
                        'AJAX': 'Y',
                    },
                    beforeSend : function() {
                        if(
                            thisComponentObject.ajaxReq != 'ToCancelPrevReq'
                            && thisComponentObject.ajaxReq.readyState < 4
                        ) {
                            thisComponentObject.ajaxReq.abort();
                        }
                    },
                    success: (result) => {
                        history.pushState({}, document.title, link);
                        thisComponentObject.$fkko.html(result);
                        thisComponentObject.init();
                    },
                });
            });
        }
    }
};