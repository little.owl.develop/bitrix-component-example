<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(false);

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?php
if ($arResult['INCLUDE_SEARCH'] == 'Y') {
    ?>
    <div class="fkko-search" data-js-id="fkko-search">
        <label class="fkko-search__label">
            Поиск
            <input class="fkko-search__input" type="text" name="fkko-search" value="<?=$_REQUEST['search']?>" />
        </label>
    </div>
    <?php
}
?>
<div class="fkko" data-js-id="fkko-component">
    <?php
    if ($arResult['AJAX'] == 'Y') {
        $APPLICATION->RestartBuffer();
    }
    ?>
    <div class="fkko-list">
        <?php
        if (!empty($arResult['ITEMS'])) {
            foreach ($arResult['ITEMS'] as $arItem) {
                ?>
                <a class="fkko-item" href="/fkko/<?=$arItem['CODE']?>/">
                    <div class="fkko-item__code">
                        <?=$arItem['CODE']?>
                    </div>
                    <div class="fkko-item__name">
                        <?=$arItem['NAME']?>
                    </div>
                </a>
                <?php
            }
        } else {
            ?>
            <div class="error"><?=Loc::getMessage('NOT_FOUND')?></div>
            <?php
        }
        ?>
    </div>
    <div data-js-id="fkko-component-pagination">
        <?=$arResult['NAV_STRING']?>
    </div>
    <?php
    if ($arResult['AJAX'] == 'Y') {
        die();
    }
    ?>
</div>

<script>
    let fkkoListCompoment = new fkkoList(<?=CUtil::PhpToJSObject($arResult)?>);
</script>

