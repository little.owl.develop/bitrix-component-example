<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

include __DIR__ . DIRECTORY_SEPARATOR . '.namespace.php';

/** @var array $componentVars */

$arComponentParameters = [
    'GROUPS' => [
        'BASE' => [
            'NAME' => Loc::getMessage($componentVars['LOC_PARAM_GROUPS'] . 'BASE'),
        ],
        'PAGINATION' => [
            'NAME' => Loc::getMessage($componentVars['LOC_PARAM_GROUPS'] . 'PAGINATION'),
        ],
        'ADDITIONAL' => [
            'NAME' => Loc::getMessage($componentVars['LOC_PARAM_GROUPS'] . 'ADDITIONAL'),
        ],
    ],
    'PARAMETERS' => [
        'INCLUDE_SEARCH' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage($componentVars['LOC_PARAMS'] . 'INCLUDE_SEARCH'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N',
        ],
        'CACHE_TIME' => [],
        'PER_PAGE' => [
            'PARENT' => 'PAGINATION',
            'NAME' => Loc::getMessage($componentVars['LOC_PARAMS'] . 'PER_PAGE'),
            'TYPE' => 'STRING',
            'DEFAULT' => '20'
        ],
        'FILTER_CODES' => [
            'PARENT' => 'ADDITIONAL',
            'NAME' => Loc::getMessage($componentVars['LOC_PARAMS'] . 'FILTER_CODES'),
            'TYPE' => 'LIST',
            'VALUES' => [],
            'ADDITIONAL_VALUES' => 'Y',
            'MULTIPLE' => 'Y',
        ],
    ],
];